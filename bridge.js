const config = require("/etc/hap/config.json")
const hap = require("hap-nodejs");
const fanService = require('./Supported/fanv2')
const temperature = require('./Supported/temperature')
const humidity = require('./Supported/humidity')
const Accessory = hap.Bridge;

const accessoryUuid = hap.uuid.generate(config.bridge.uuid);
const accessory = new Accessory(config.bridge.name, accessoryUuid);

var mqtt = require('mqtt');
var client = mqtt.connect("mqtt://"+config.mqtt.ip,{clientId:"mqttjs01"})

client.on('message',function(topic, message, packet){
    if (topic == "/feeds/temperature") {
        temperature.setTemperature(parseFloat(message))
    }
    if (topic == "/feeds/humidity") {
        humidity.setHumidity(parseFloat(message));
    }
});

client.on("connect",function() {
    console.log("connected");
    accessory.addService(temperature.temperatureSensor(client));
    accessory.addService(humidity.humiditySensor(client));
    accessory.addService(fanService(client));
    accessory.publish({
        username: config.bridge.macUsername,
        pincode: config.bridge.pinCode,
        port: config.bridge.port,
        category: hap.Categories.BRIDGE, // value here defines the symbol shown in the pairing screen
    });
    console.log("Accessory setup finished!");
})
