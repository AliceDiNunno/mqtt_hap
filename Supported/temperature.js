const hap = require("hap-nodejs");
const Characteristic = hap.Characteristic;
const CharacteristicEventTypes = hap.CharacteristicEventTypes;
const Service = hap.Service;

var temperature = 0
function temperatureSensor(client) {
    client.subscribe("/feeds/temperature",{qos:1});
    const temperatureService = new Service.TemperatureSensor("Temperature");

    const temperatureCharacteristic = temperatureService.getCharacteristic(Characteristic.CurrentTemperature);

    temperatureCharacteristic.on(CharacteristicEventTypes.GET, (callback) => {
        let currentTemperature = temperature;
        console.log("Queried current brightness level: " + currentTemperature);
        callback(undefined, currentTemperature);
    });

    return temperatureService
}

function setTemperature(tmp) {
    temperature = tmp;
}

module.exports = { temperatureSensor, setTemperature };
