const hap = require("hap-nodejs");
const Characteristic = hap.Characteristic;
const CharacteristicEventTypes = hap.CharacteristicEventTypes;
const Service = hap.Service;

function fanService(client) {
    const fanService = new Service.Fanv2("Fan");
    const speedCharacteristic = fanService.getCharacteristic(Characteristic.RotationSpeed);
    const swingCharacteristic = fanService.getCharacteristic(Characteristic.SwingMode);

    var fanState = 0
    speedCharacteristic.on(CharacteristicEventTypes.GET, callback => {
        console.log("Queried current fan state: " + fanState);
        callback(undefined, fanState);
    });
    speedCharacteristic.on(CharacteristicEventTypes.SET, (value, callback) => {
        console.log("Setting fan state to: " + value);

        fanState = value;

        if (fanState == 0) {
            client.publish('/feeds/fan/speed', "FS0");
        }

        if (fanState > (100/3)*0 && fanState < (100/3)*1 ) {
            client.publish('/feeds/fan/speed', "FS1");
        }

        if (fanState > (100/3)*1 && fanState < (100/3)*2 ) {
            client.publish('/feeds/fan/speed', "FS2");
        }

        if (fanState > (100/3)*2 && fanState < (100/3)*3 ) {
            client.publish('/feeds/fan/speed', "FS3");
        }

        callback();
    });


    var swingState = false
    swingCharacteristic.on(CharacteristicEventTypes.GET, (callback) => {
        console.log("Queried current swing level: " + swingState);
        callback(undefined, swingState);
    });
    swingCharacteristic.on(CharacteristicEventTypes.SET, (value, callback) => {
        console.log("Setting swing level to: " + value);
        swingState = value;

        client.publish('/feeds/fan/swing', swingState ? "on" : "off");

        callback();
    });

    return fanService;
}

module.exports = fanService;
