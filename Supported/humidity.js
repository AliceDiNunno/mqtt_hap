const hap = require("hap-nodejs");
const Characteristic = hap.Characteristic;
const CharacteristicEventTypes = hap.CharacteristicEventTypes;
const Service = hap.Service;

var humidity = 0
function humiditySensor(client) {
    client.subscribe("/feeds/humidity",{qos:1});
    const humiditySensor = new Service.HumiditySensor("Humidity");

    const humidityCharacteristic = humiditySensor.getCharacteristic(Characteristic.CurrentRelativeHumidity);

    humidityCharacteristic.on(CharacteristicEventTypes.GET, (callback) => {
        let currentHumidity = humidity;

        console.log("Queried current humidity level: " + currentHumidity);
        callback(undefined, currentHumidity);
    });

    return humiditySensor
}

function setHumidity(hmdt) {
    humidity = hmdt;
}

module.exports = { humiditySensor, setHumidity };
