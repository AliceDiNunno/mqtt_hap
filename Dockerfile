FROM alpine:latest
WORKDIR /usr/src/app
COPY package*.json ./

RUN apk add --no-cache git python2 python3 make g++ avahi-compat-libdns_sd avahi-dev dbus iputils sudo nano nodejs npm

RUN npm install
COPY . .

CMD [ "node", "bridge.js" ]

